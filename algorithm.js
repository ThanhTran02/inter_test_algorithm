function miniMaxSum(arr) {
  const sum = arr.reduce((acc, curr) => acc + curr, 0);
  const min = Math.min(...arr);
  const max = Math.max(...arr);

  const minSum = sum - max;
  const maxSum = sum - min;

  console.log(minSum, maxSum);
}

function findMin(arr) {
  const min = Math.min(...arr);
  console.log(`Min:${min}`);
}

function getTotal(arr) {
  const total = arr.reduce((sum, num) => sum + num, 0);
  console.log(`Count total of array:${total}`);
}

function findMax(arr) {
  const max = Math.max(...arr);
  console.log(`Max:${max}`);
}
function findEvenElements(arr) {
  const evenElements = arr.filter((num) => num % 2 === 0);
  console.log(`Even elements in array:${evenElements}`);
}

function findOddElements(arr) {
  const oddElements = arr.filter((num) => num % 2 !== 0);
  console.log(`Odd elements in array:${oddElements}`);
}

process.stdin.resume();
process.stdin.setEncoding("ascii");
let _input = "";
let valueCount = 0;

process.stdin.on("data", function (input) {
  _input += input.replace(/\r/g, "");
  valueCount++;

  if (valueCount === 5) {
    process.stdin.pause();
    process.stdin.emit("end");
  }
});

process.stdin.on("end", function () {
  const values = _input.trim().split("\n");
  const arr = values.map((str) => parseInt(str, 10));

  miniMaxSum(arr);
  //Bouns
  // findMin(arr);
  // findMax(arr);
  // findEvenElements(arr);
  // findOddElements(arr);
  // getTotal(arr);
  process.exit();
});
